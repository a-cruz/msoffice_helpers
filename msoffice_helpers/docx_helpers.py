import re
import json

from typing import Optional

import docx # type: ignore
from docx.oxml.ns import nsdecls # type: ignore
from docx.oxml import parse_xml # type: ignore
from docx.shared import Inches # type: ignore
from docx.enum.text import WD_ALIGN_PARAGRAPH # type: ignore

# HELPER FUNCTIONS
def cell_shader(cell, rgb_color):
    shading_elm = parse_xml(r'<w:shd {} w:fill="{}"/>'.format(nsdecls('w'), rgb_color))
    cell._tc.get_or_add_tcPr().append(shading_elm)

def delete_paragraph(paragraph: docx.text.paragraph.Paragraph) -> None:
	'''
	Function to delete a given paragraph in a Word document.
	Requires as input the paragraph object.
	'''
	p = paragraph._element
	p.getparent().remove(p)
	p._p = p._element = None

def delete_1st_table_paragraph(table: docx.table.Table) -> None:
    '''
    By default, Word creates a leading empty paragraph in every table.
    This function deletes it to removed the spacing.
    '''
    row_zero: docx.table._Row = table.rows[0]
    cell_zero: docx.table._Cell = row_zero.cells[0]
    paragraph_zero: docx.text.paragraph.Paragraph = cell_zero.paragraphs[0]
    if paragraph_zero == "": # Don't delete it if it isn't empty
        delete_paragraph(paragraph_zero)
        
def get_table_dimensions(table_model: dict) -> tuple[int,int]:
    '''
    Get the number of rows & columns in a table model
    '''
    height: int = len(table_model['table']['rows'])
    # Loop through row cells to get table width
    width: int = 0
    for row in table_model['table']['rows']:
        if len(row['cells']) > width:
            width = len(row['cells'])
    return (height,width)

def locate_text(doc_obj: docx.document.Document, search: str) -> int:
    '''
    Function to find a string in a Word document and return the paragraph
    index. Receives as input the Word document object (using docx module) and
    a search string.
    '''
    found: bool=False
    for index, paragraph in enumerate(doc_obj.paragraphs):
        if re.match(search, paragraph.text):
            found = True
            break
    if not found:
        raise ValueError(f"Could not locate '{search}' in the document. Correct your search or the document and try again.")
    return index

def set_col_width(table: docx.table.Table, column: int, width: int) -> None:
    col_width: docx.shared.Inches = Inches(float(width))
    for row in table.rows:
        row.cells[column].width = col_width

def table_model_validation(table_model: dict) -> None:

    def print_sample_schema() -> None:
        sample_schema: dict = {
            "table": {
                "style": "plain",
                "rows": [
                    {
                        "cells": [
                            {
                                "background": None,
                                "paragraphs": [{"style": "No Spacing", "text": "Sample Text"}]
                            }
                        ]
                    }
                ]
            }
        }
        print("**********************************************************************************")
        print("******************************** SAMPLE SCHEMA ***********************************")
        print("**********************************************************************************")
        print(json.dumps(sample_schema, indent=4))
        print("Refer to https://gitlab.com/a-cruz/msoffice_helpers for a more comprehensive example of table schemas.")
        print("**********************************************************************************")

    if not isinstance(table_model, dict):
        raise TypeError(f"A table model of type dictionary must be supplied but received object of type '{type(table_model)}'")
    else:
        # Validate table model schema
        if not len(list(table_model.keys())) in [1, 2]:
            print_sample_schema()
            raise ValueError("Invalid Table Schema Detected. Table model should have one or two keys but zero or greater than two keys detected.")
        elif not "table" in table_model:
            print_sample_schema()
            raise ValueError("Invalid Table Schema Detected. Table model should have a single 'table' key but 'table' key not found.")
        elif not "rows" in table_model['table']:
            print_sample_schema()
            raise ValueError("Invalid Table Schema Detected. Table model should have a 'rows' key in the table value but 'rows' key not found.")
        elif not isinstance(table_model['table']['rows'], list):
            print_sample_schema()
            raise TypeError(f"Invalid Table Schema Detected. Table model row container should be of type 'list' but type '{type(table_model['table']['rows'])}' detected.")
        else:
            # Loop through the rows to continue schema validation
            for row in table_model['table']['rows']:
                if not isinstance(row, dict):
                    print_sample_schema()
                    raise TypeError(f"Invalid Table Schema Detected. Table model rows should be of type 'dict' but type '{row}' detected.")
                elif not len(list(row.keys())) == 1:
                    print_sample_schema()
                    raise ValueError("Invalid Table Schema Detected. Table model row should have a single 'cells' key but zero or multiple keys detected.")
                elif not "cells" in row:
                    print_sample_schema()
                    raise ValueError(f"Invalid Table Schema Detected. Table model rows should have a single 'cells' key but 'cells' key not found.")
                elif not isinstance(row['cells'], list):
                    print_sample_schema()
                    raise TypeError(f"Invalid Table Schema Detected. Table model cells container should be of type 'list' but type '{type(row['cells'])}' detected.")
                else:
                    # Loop through this row's cells to continue schema validation
                    for cell in row['cells']:
                        if not isinstance(row, dict):
                            print_sample_schema()
                            raise TypeError(f"Invalid Table Schema Detected. Table model cells should be of type 'dict' but type '{cell}' detected.")
                        elif not list(set(list(cell.keys())).intersection(["paragraphs", "table", "merge"])):
                            print_sample_schema()
                            raise ValueError(f"Invalid Table Schema Detected. Table model cells should have one of the following keys: ['table', 'paragraphs', 'merge'] but none detected.")
                        if "paragraphs" in cell:
                            if not isinstance(cell['paragraphs'], list):
                                print_sample_schema()
                                raise TypeError(f"Invalid Table Schema Detected. Table model paragraphs should be of type 'list' but type '{cell['paragraphs']}' detected.")
                            elif cell['paragraphs']:
                                # Loop through the paragraphs to continue schema validation
                                for para in cell['paragraphs']:
                                    if not isinstance(para, dict):
                                        print_sample_schema()
                                        raise TypeError(f"Invalid Table Schema Detected. Table model paragraphs should be of type 'dict' but type '{para}' detected.")
                                    elif not "text" in para:
                                        print_sample_schema()
                                        raise ValueError(f"Invalid Table Schema Detected. Table model paragraphs should have a 'text' key but 'text' key not found.")
                                    elif not isinstance(para['text'], (str, int, list)):
                                        print_sample_schema()
                                        raise TypeError(f"Invalid Table Schema Detected. Table model paragraph text should be of one of type ['str', 'int', 'list'] but type {type(para['text'])} detected.")
                
# MAIN FUNCTIONS                                 
def build_table(doc_obj: docx.document.Document, table_model: dict, del_para: Optional[bool]=True) -> docx.table.Table:
    '''
    Converts a properly-formatted Python dictionary into a Word table object.
    '''
    table_model_validation(table_model)
    for k, v in table_model.items():
        if k == "table":
            # Create a new table in the Word doc with the same dimensions as the table model
            dimensions: tuple[int,int] = get_table_dimensions(table_model)
            table: docx.table.Table = doc_obj.add_table(dimensions[0],dimensions[1])
            table.style = v["style"]
            table.delpara = [status["delpara"] for status in v if 'delpara' in v.keys()]
            # Iterate model rows
            for row_index, row_data in enumerate(v["rows"]):
                current_row: docx.table._Row = table.rows[row_index]
                # Iterate model row's cells
                for cell_index, cell_data in enumerate(row_data["cells"]):
                    current_cell: docx.table._Cell
                    if "merge" in cell_data.keys():
                        current_cell.merge(current_row.cells[cell_index])
                    else:
                        current_cell = current_row.cells[cell_index]
                        if "width" in cell_data.keys(): # Hard-code the cell width
                            set_col_width(table, cell_index, cell_data['width'])
                        if "background" in cell_data.keys(): # Set the cell background color
                            cell_shader(current_cell, cell_data["background"])
                        if "paragraphs" in cell_data.keys(): # Populate the cell with paragraphs
                            for para_index, para in enumerate(cell_data["paragraphs"]):
                                # Add paragraph
                                if para_index + 1 > len(current_cell.paragraphs):
                                    current_cell.add_paragraph()
                                # Style the paragraph
                                p: docx.text.paragraph.Paragraph = current_cell.paragraphs[para_index]
                                p.style = para["style"]
                                # Align paragraph text
                                if "alignment" in para.keys():
                                    if para['alignment'] == "center":
                                        p.alignment = WD_ALIGN_PARAGRAPH.CENTER
                                    if para['alignment'] == "right":
                                        p.alignment = WD_ALIGN_PARAGRAPH.RIGHT
                                # Populate cell with single-line text
                                if isinstance(para['text'], str | int):
                                    # NOTE: Integers are auto-converted to strings
                                    p.text = str(para['text'])
                                # Populate cell with multi-line text
                                elif isinstance(para['text'], list):
                                    for i, element in enumerate(para['text']):
                                        # NOTE: Integers are auto-converted to strings
                                        run = p.add_run(str(element))
                                        if i < len(para["text"]) - 1: # Don't add another line break on the last line
                                            run.add_break()
                        else: # if no paragraphs are in the current model cell, delete the auto-generated paragraph zero
                            delete_paragraph(current_cell.paragraphs[0])
                        if "table" in cell_data.keys(): # Nested table, recurse through build function
                            build_table(current_cell, cell_data, del_para=False)
    if del_para:
        delete_1st_table_paragraph(table)
    return table

def replace_placeholder_with_table(doc_obj: docx.document.Document, placeholder: str, table: docx.table.Table) -> None:
	'''
	Function to relocate a Word table object to immediately follow a given
	reference paragraph identified by the placeholder. Receives as input the 
	placeholder string and the Word table object (using docx module).
	After moving the Word table after the placeholder paragraph, delete the
	placeholder paragraph.
	'''
	# Locate the paragraph number of the supplied placeholder text
	paragraph: int = locate_text(doc_obj, placeholder)
	placeholder_loc: docx.text.paragraph.Paragraph = doc_obj.paragraphs[paragraph]

	# Move our new Word table to a new paragraph immediately after the placeholder paragraph
	tbl, p = table._tbl, placeholder_loc._p
	p.addnext(tbl)
	
    # Delete the placeholder paragraph
	delete_paragraph(placeholder_loc)