# 0.1.1 (December 4, 2023)
- Added table validation to validate user-provided table models match the expected table model schema

# 0.1.0 (December 1, 2023)
- First commit