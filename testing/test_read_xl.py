import sys

import openpyxl
import pytest

# setting path for import from parent folder
sys.path.append('../')

from msoffice_helpers.openpyxl_helpers import extract_data

test_workbook = "wb.xlsm"

def load_sheet(sheet_name):
    wb = openpyxl.load_workbook(test_workbook)
    sheet = wb[sheet_name]
    wb.close()
    return sheet

def test_left_lookup():
    sheet = load_sheet("Tenants")
    results = extract_data(sheet, "INTEGER1", "left-lookup")
    assert results == "STRING1"

def test_right_lookup():
    sheet = load_sheet("Tenants")
    results = extract_data(sheet, "STRING1", "right-lookup")
    assert results == "INTEGER1"

def test_table_lookup():
    sheet = load_sheet("Tenants")
    results = extract_data(sheet, "STRING1")
    assert results == [
            {
                'STRING1': 'Row 1 String',
                'INTEGER1': 1,
                'MULTI-LINE STRING':
                'Row1\nmulti-line\nstring'
            },
            {
                'STRING1': 'Row 2 String',
                'INTEGER1': 2,
                'MULTI-LINE STRING':
                'Row2\nmulti-line\nstring'
            },
            {
                'STRING1': 'Row 3 String',
                'INTEGER1': 3,
                'MULTI-LINE STRING':
                'Row3\nmulti-line\nstring'
            }
        ]
    
def test_invalid_sheet_input():
    with pytest.raises(TypeError) as excinfo:
        extract_data("Invalid Sheet", "INTEGER1", "left-lookup")
    assert str(excinfo.value) == "Input sheet should be of type 'openpyxl.worksheet.worksheet.Worksheet' but is type <class 'str'>."

def test_invalid_keyword_input():
    sheet = load_sheet("Tenants")
    with pytest.raises(TypeError) as excinfo:
        extract_data(sheet, 1, "left-lookup")
    assert str(excinfo.value) == "Input keyword should be of type 'str' but is type <class 'int'>."

def test_keyword_not_found():
    sheet = load_sheet("Tenants")
    with pytest.raises(ValueError) as excinfo:
        extract_data(sheet, "A MISSING KEYWORD", "left-lookup")
    assert str(excinfo.value) == "Keyword 'A MISSING KEYWORD' was not found in sheet 'Tenants'."

def test_invalid_lookup_type():
    sheet = load_sheet("Tenants")
    with pytest.raises(ValueError) as excinfo:
        extract_data(sheet, "STRING1", "invalid-lookup")
    assert str(excinfo.value) == "Lookup type should be one of ['table-lookup', 'left-lookup', 'right-lookup'] but 'invalid-lookup' supplied. Please provide a valid lookup type."