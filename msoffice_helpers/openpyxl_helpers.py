from typing import Optional

import openpyxl # type: ignore

def validate_input(sheet: openpyxl.worksheet.worksheet.Worksheet, keyword: str, lookup_type: Optional[str]) -> None:
    if not isinstance(sheet, openpyxl.worksheet.worksheet.Worksheet):
        raise TypeError(f"Input sheet should be of type 'openpyxl.worksheet.worksheet.Worksheet' but is type {type(sheet)}.")
    if not isinstance(keyword, str):
        raise TypeError(f"Input keyword should be of type 'str' but is type {type(keyword)}.")
    if not lookup_type in ["table-lookup", "left-lookup", "right-lookup"]:
        raise ValueError(f"Lookup type should be one of ['table-lookup', 'left-lookup', 'right-lookup'] but '{lookup_type}' supplied. Please provide a valid lookup type.")
    
def locate_keyword_coordinates(sheet: openpyxl.worksheet.worksheet.Worksheet, keyword: str) -> str:

    # LOCATE KEYWORD IN SHEET
    discovered_coordinates = []
    for row in sheet.iter_rows(): # Loop through rows in the Excel sheet
        for cell in row: # Loop through columns (cells) in the row
            if cell.value and cell.value == keyword:
                discovered_coordinates.append(cell.coordinate)

    # VALIDATE RESULTS
    if len(discovered_coordinates) > 1: # Invalid sheet, multiple instances of the keyword found
        raise ValueError(f"Multiple instances of keyword '{keyword}' was found in sheet '{sheet.title}'. Please ensure keywords are uniue in the sheet.")
    elif len(discovered_coordinates) == 0: # Invalid sheet, keyword not found
        raise ValueError(f"Keyword '{keyword}' was not found in sheet '{sheet.title}'.")
    
    return discovered_coordinates[0]

def extract_table(sheet: openpyxl.worksheet.worksheet.Worksheet, coordinates: str) -> list:
    '''
    Function to build a table (dict) of data. Using keyword coordinates as a
    reference point. Searches down, left, and right of the reference coords
    and builds a table of all data (list of flat dictionaries) ending when it
    encounters the first empty cell (assumes all data is populated contiguously)
    '''
    table: list = []
    # Establish points of reference
    header_row: int = sheet[coordinates].row
    data_row: int = sheet[coordinates].row + 1
    keyword_column: int = sheet[coordinates].column

    # Establish column range/table width by discovering header text
    col_range: list[int] = [keyword_column,keyword_column]
    left_current_column: int = keyword_column
    while True: # Begin Searching Left of Keyword
        left_cell_content: str = sheet.cell(header_row,left_current_column).value
        if not left_cell_content: # Encountered empty cell, exit left boundary discovery
            col_range[0] = left_current_column + 1
            break
        else:
            left_current_column -= 1
    right_current_column: int = keyword_column
    while True: # Begin Searching Right of Keyword
        try:
            right_cell_content: str = sheet.cell(header_row,right_current_column).value
        except IndexError:
            col_range[1] = right_current_column
            break
        if not right_cell_content: # Encountered empty cell, exit right boundary discovery
            col_range[1] = right_current_column
            break
        else:
            right_current_column += 1

    col_start: int = col_range[0]
    col_end: int = col_range[1]
    # Loop through the rows and columns to build the table
    while True:
        cell_content: str | None
        try:
            cell_content = sheet.cell(data_row,keyword_column).value
        except IndexError:
            cell_content = None
        if not cell_content: # Empty cell detected, exit table build
            break
        else:
            # Build dictionary for this row using header row as the keys
            this_row_dict: dict = {}
            for col in range(col_start,col_end):
                dict_key: str = sheet.cell(header_row,col).value
                dict_val: str = sheet.cell(data_row,col).value
                this_row_dict[dict_key] = dict_val
            table.append(this_row_dict)
        data_row += 1 # All done with this row, move to next row
    return table

def extract_single_cell(sheet: openpyxl.worksheet.worksheet.Worksheet, coordinates: str, lookup_type: Optional[str]) -> str:
    # Define offset (left or right of coordinates depending on lookup type)
    offset: int
    if lookup_type == "left-lookup":
        offset = -1
    elif lookup_type == "right-lookup":
        offset = +1

    # Establish coordinates of desired data (cell)
    row: int = sheet[coordinates].row
    column: int = sheet[coordinates].column + offset

    # Return the cell contents
    return sheet.cell(row,column).value

def extract_data(sheet: openpyxl.worksheet.worksheet.Worksheet, keyword: str, lookup_type: Optional[str]="table-lookup") -> list | str:

    validate_input(sheet, keyword, lookup_type)
    
    coordinates: str = locate_keyword_coordinates(sheet, keyword)

    return_data: list | str
    if lookup_type == "table-lookup":
        return_data = extract_table(sheet, coordinates)
    else:
        return_data = extract_single_cell(sheet, coordinates, lookup_type)

    return return_data