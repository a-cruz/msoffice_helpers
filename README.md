# msoffice_helpers
This project is a Python package with helper functions for Python libraries used to read and write Microsoft Office files (Word & Excel).

In several different projects I use [openpyxl](https://openpyxl.readthedocs.io/en/stable/) to extract data from Excel sheets or [python-docx](https://python-docx.readthedocs.io/en/latest/) to write tables to Word documents.

This project's helper functions provide the following abstractions for those libraries:
- **openpyxl**: A worksheet search function to find data in a sheet
    - Search a specific Excel sheet for a keyword and return the value of the cell to the left (left-lookup), the value of the cell to the right (right-lookup), or build a python "table" (list of dictionaries) containing all data adjacent to the keyword (table-lookup).
- **python-docx**: A table build function
    - So that you don't have to remember how to use python-docx for every new project this helper function allows you to model a Word table using a Python dictionary (slightly easier to remember or re-remember), and the helper function converts the model to the proper python-docx objects. The table model conversion supports styles, background coloring, static cell width, merging cells, and nested tables.
- **python-docx**: A function to swap placeholder text with a table object in Word
    - A common use-case for me is creating as-built documentation in Word where I start with a Word document template. In the template I have many placeholders where I want to inject certain data tables created with the table build function. This helper function will search the template for the placeholder keyword and replace the placeholder text with the data table.

# PROJECT FOLDER STRUCTURE
```
├── artwork                      ---Folder that contains images for the README file
├── testing                      ---Folder that contains unit test artifacts
|    ├── requirements.txt        ---Python packages required for running unit tests
|    ├── template.docx           ---Sample Word template read by unit tests
|    ├── test_build_doc.py       ---Unit tests for build_table helper function
|    ├── test_read_xl.py         ---Unit tests for excel worksheet search helper function
|    └── wb.xlsm                 ---Sample Excel workbook read by unit tests
├── __init__.py                  ---Identifies the project folder as a Python package
├── .gitignore                   ---The GIT ignore file
├── CHANGELOG.md                 ---Tracks changes in the project
├── docx_helpers.py              ---Helper functions for the python-docx Python library
├── openpyxl_helpers.py          ---Helper functions for the openpyxl Python library
└── README.md                    ---This file
```

# REQUIREMENTS
- **Python3.10 or greater** (Verified on 3.11.4, 3.10.12)
- If docx_helpers will be used in your project, **python-docx** is required
- If openpyxl_helpers will be used in your project, **openpyxl** is required

The required python packages can be installed with:

```bash
pip install python-docx
pip install openpyxl
```

# ADDING THE PACKAGE TO YOUR PROJECT
The best way to include the package to your project is to add it as a submodule to your GIT project.  
That way you can update the helper functions independent of your project.  
To add this Python package as a submodule to your GIT project, do the following from your project folder:
```bash
git submodule add https://gitlab.com/a-cruz/msoffice_helpers
```

**NOTE:** When cloning your project, GIT will **_not_** include submodules by default. In order to include submodules when you clone your project do:
```bash
git clone --recurse-submodules <your_project_git_repo_url>
```

# USING DOCX_HELPERS.PY MODULE
docx_helpers has two main functions available to your scripts:
1. build_table(<doc_obj>, <table_model>, <del_para>)
1. replace_placeholder_with_table(<doc_obj>, <search_string>, <table_obj>)

Import the helper functions from the module into your script with:
```python
from msoffice_helpers.docx_helpers import build_table, replace_placeholder_with_table
```

### build_table(<doc_obj>, <table_model>, <del_para>)
The purpose of this module is to allow the script author to model Word tables using Python dictionaries. If formatted properly, the module will translate the Python dictionary to the appropriate python-docx syntax and create the Word table object.

The build_table function has the following parameters:
- **<doc_obj>** - The python-docx Word document object (once created in your script).
- **<table_model>** - The Word table model (Python dictionary). The expected Python dictionary format to model a basic Word table is:
    ```python
    {
        "table": {
            "style": None,
            "rows": [
                {
                    "cells": [
                        {
                            "width": None,
                            "background": None,
                            "paragraphs": [{"style":None,"alignment": "center", "text":"Row 0 Column 0"}],
                            "table": {optional child table}
                        },
                        {
                            "width": None,
                            "background": None,
                            "paragraphs": [{"style":None,"alignment": "center", "text":"Row 0 Column 1"}],
                            "table": {optional child table}
                        },
                    ]
                },
                {
                    "cells": [
                        {
                            "width": None,
                            "background": None,
                            "paragraphs": [{"style":None,"alignment": "center", "text":"Row 1 Column 0"}],
                            "table": {optional child table}
                        },
                        {
                            "merge": None # This will merge columns 0 & 1 of row 1
                        }
                    ]
                }
            ]
        }
    }
    ```
    The cell **background** attribute is optional. If supplied with a hexidecimal color code, the cell will be shaded that color.
    
    The cell **width** attribute is optional. If supplied with a decimal number it will hard-code that column's width to the supplied value.

    The cell **table** attribute is optional. It can be used to nest tables within table cells.

    The paragraph **style** attribute is required. If set to anything besides None it will use the Word style referenced. The style must already exist in the source/template Word document.

    The paragraph **alignment** attribute is optional. If set to ```"center"``` it will center-align the text within a cell, if set to ```"right"``` it will right-align the text within a cell. If not set the text will default to left-align.

    The cell **merge** attribute is optional. If used the cell will be merged with the cell above (to the left). Multiple merges can be used in a row to merge multiple cells.

    By default a paragraph's **text** property will create a single-line (but wrapped) entry in the cell if the value is a string. If you would like to create a multi-line cell entry, supply the value as a list instead of a string. This will instruct the helper function to add a line break after each list item.  
    NOTE: integers supplied in the cell value will be converted to strings by the helper function.
- **<del_para>** - This is an optional boolean parameter. If not set it will default to True. MS Word tables when created, automatically have an empty paragraph at the top/beginning of the table cell. This can create unwanted spacing at the top of the table. By default (value set to "True") the paragraph will be deleted. If you want to keep the paragraph (to add text to it), set this to False.

**IMPORTANT NOTE:** This helper function does **_NOT_** add the table object to your Word file or add the table to your document object. It only creates the Word table object as a standalone entity. 

To add the table object to your document object you have two options:
1. Manually using python-docx functions (For detailed documentation of the python-docx library see [python-docx](https://python-docx.readthedocs.io/en/latest/))
1. Use the replace_placeholder_with_table() helper function detailed in the next sub-section.

### replace_placeholder_with_table(<doc_obj>, \<placeholder\>, \<table\>)
The purpose of this module is to search a Word Document for a given string (the placeholder) and replace the string with a Word table object.

The replace_placeholder_with_table function has the following arguments:
- **\<doc_obj\>** - The python-docx Word document object (read in from the template file for example)
- **\<placeholder\>** - The string to search for in the document object (doc_obj)
- **\<table\>** - The python-docx Word Table object that will replace the \<placeholder\> in the document object (odc_obj)

### EXAMPLE
We start with a Microsoft Word template named "source-template.docx" that looks like this:

![Word Template](artwork/word_template.jpg)

Our sample Python script looks like this:
```python
from docx import Document
from msoffice_helpers.docx_helpers import build_table, replace_placeholder_with_table

doc_obj = Document("source-template.docx")

my_dictionary = {
    "table": {
        "style": None,
        "rows": [
            {
                "cells": [
                    {
                        "paragraphs": [],
                        "table": {
                            "style": "plain",
                            "rows": [
                                {
                                    "cells": [
                                        {
                                            "background": "506279",
                                            "paragraphs":[{"style": "regularbold", "text": "Header 1:"}]
                                        },
                                        {
                                            "background": "506279",
                                            "paragraphs":[{"style": "regularbold", "text": "Header 2:"}]
                                        },
                                        {
                                            "background": "506279",
                                            "paragraphs":[{"style": "regularbold", "text": "Header 3:"}]
                                        }
                                    ]
                                },
                                {
                                    "cells": [
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 1 Data 1:"}]
                                        },
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 1 Data 2:"}]
                                        },
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 1 Data 3:"}]
                                        }
                                    ]
                                },
                                {
                                    "cells": [
                                        {
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 2 Data 1:"}]
                                        },
                                        {
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 2 Data 2:"}]
                                        },
                                        {
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 2 Data 3:"}]
                                        }
                                    ]
                                },
                                {
                                    "cells": [
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 3 Data 1:"}]
                                        },
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 3 Data 2:"}]
                                        },
                                        {
                                            "background": "D5DCE4",
                                            "paragraphs":[{"style": "No Spacing", "text": "Row 3 Data 3:"}]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        ]
    }
}

my_table = build_table(doc_obj, my_dictionary)

replace_placeholder_with_table(doc_obj, "{{py_placeholder}}", my_table)

doc_obj.save("output_word_doc.docx")
```

We run the Python script and it produces a new Word document named "output_word_doc.docx" that looks like this:

![Word Template](artwork/word_output.jpg)

# USING OPENPYXL_HELPERS.PY MODULE
openpyxl_helpers exposes one Python function to your scripts:

- extract_data(\<sheet\>, \<keyword\>, <lookup_type>)

The purpose of the extract_data helper function is to pull "unknown" or "dynamic" data from an Excel sheet based on a known/static keyword.  
The helper function can pull data from a cell immediately to the left of the keyword, immediately to the right of the keyword, or it can build an entire table from all data adjacent to the keyword.

Here is a detailed explanation of the extract_data parameters:

- **\<sheet\>** - The openpyxl sheet object
- **\<keyword\>** - The keyword to search for in the sheet.  
    **NOTE:** It's a good idea to use static text from a table header for the search and the keyword value must be unique on the sheet. If the module detects multiple hits on the keyword search it will fail and raise an exception.
- **<lookup_type>** - This is an optional parameter. If not supplied a "table-lookup" will be performed by default. The following are the only parameter arguments accepted:
    **table-lookup** This is the default if no argument is supplied for this parameter. A table lookup locates the keyword in the sheet, it then begins searching the cells to the left of the keyword, to the right of the keyword, and down from the keyword until it encounters an empty cell. It then builds a Python model of the data. The model is a list of dictionaries where each list item represents a row. The row is a dicionary of key value pairs where the key is the header (from row 0) and the value is the data.
    **right-lookup** Will search for the keyword and return a single value (instead of a table) from the cell immediately to the right of the keyword.  
    **left-lookup** Will search for the keyword and return a single value (instead of a table) from the cell immediately to the left of the keyword.  

### EXAMPLE
We start with an excel workbook named "workbook.xlsm" that looks like this:

![excel workbook](artwork/xl_sheet.jpg)

Our sample Python script looks like this:
```python
import openpyxl
from msoffice_helpers.openpyxl_helpers import extract_data
import json

wb = openpyxl.load_workbook("workbook.xlsm")
sheet = wb['Sheet1']
wb.close()

tenants = extract_data(sheet, "TENANT NAME")
fabric_name = extract_data(sheet, "Fabric Name:", "right-lookup")

print("TENANT TABLE:")
print(json.dumps(tenants,indent=4))
print("\nFABRIC NAME:")
print(json.dumps(fabric_name,indent=4))
```

When the Python script is ran, we get the following output:
```
TENANT TABLE:
[
    {
        "TENANT NAME": "Tenant1",
        "DESCRIPTION": "My 1st Tenant"
    },
    {
        "TENANT NAME": "Tenant2",
        "DESCRIPTION": "My 2nd Tenant"
    }
]

FABRIC NAME:
"My Fabric"
```