import sys
import json

from docx import Document

# setting path for import from parent folder
sys.path.append('../')

from msoffice_helpers.docx_helpers import build_table, replace_placeholder_with_table

# GLOBAL SCRIPT CONFIGURATION VARIABLES
template_file = "template.docx"
output_file = "test_generated_doc.docx"
table_header_color = "506279"
alternating_row_color = "D5DCE4"

test_model = {
    "table": {
            "style": "plain",
            "rows": [
                {
                    "cells": [
                        {
                            "background": table_header_color,
                            "paragraphs": [{"style": "regularbold", "text": "Column 1 Header:"}]
                        },
                        {
                            "background": table_header_color,
                            "paragraphs": [{"style": "regularbold", "text": "Column 2 Header:"}]
                        },
                        {
                            "background": table_header_color,
                            "paragraphs": [{"style": "regularbold", "text": "Column 3 Header:"}]
                        },
                        {
                            "merge": None
                        }
                    ]
                },
                {
                    "cells": [
                        {
                            "paragraphs": [{"style": "No Spacing", "text": "String Data"}]
                        },
                        {
                            "paragraphs": [{"style": "No Spacing", "text": 7}]
                        },
                        {
                            "paragraphs": [{"style": "No Spacing", "text": ["Multi","line","Data"]}]
                        },
                        {
                            "merge": None
                        }
                    ]
                },
                {
                    "cells": [
                        {
                            "paragraphs": [],
                            "table": {
                                "style": "plain",
                                "rows": [
                                    {
                                        "cells": [
                                            {
                                                "background": "506279",
                                                "paragraphs":[{"style": "regularbold", "text": "Header 1:"}]
                                            },
                                            {
                                                "background": "506279",
                                                "paragraphs":[{"style": "regularbold", "text": "Header 2:"}]
                                            }
                                        ]
                                    },
                                    {
                                        "cells": [
                                            {
                                                "paragraphs":[{"style": "No Spacing", "text": "Data 1"}]
                                            },
                                            {
                                                "paragraphs":[{"style": "No Spacing", "text": "Data 2"}]
                                            }
                                        ]
                                    }
                                ]
                            }
                        }
                    ]
                }
            ]
        }
    }

def test_build_table():
    doc_obj = Document(template_file)
    word_table = build_table(doc_obj, test_model)
    # Validate parent table dimensions (3x4)
    assert len(word_table.rows) == 3
    assert len(word_table.rows[0].cells) == 4
    # Validate parent table content
    assert word_table.rows[0].cells[0].text == "Column 1 Header:"
    assert word_table.rows[0].cells[1].text == "Column 2 Header:"
    assert word_table.rows[0].cells[2].text == "Column 3 Header:"
    assert word_table.rows[0].cells[3].text == "Column 3 Header:" # This is a merged cell, has same content as the adjacent cell
    assert word_table.rows[1].cells[0].text == "String Data"
    assert word_table.rows[1].cells[1].text == "7"
    assert word_table.rows[1].cells[2].text == "Multi\nline\nData"
    assert word_table.rows[1].cells[3].text == "Multi\nline\nData" # Merged cell
    # assert word_table.rows[2].cells[0].text == "\n"
    assert word_table.rows[2].cells[1].text == ""
    assert word_table.rows[2].cells[2].text == ""
    assert word_table.rows[2].cells[3].text == ""
    # Validate child table exists and child table dimensions (2x2)
    assert len(word_table.rows[2].cells[0].tables) == 1
    assert len(word_table.rows[2].cells[0].tables[0].rows) == 2
    assert len(word_table.rows[2].cells[0].tables[0].rows[0].cells) == 2
    # Validate child table content
    assert word_table.rows[2].cells[0].tables[0].rows[0].cells[0].text == "Header 1:"
    assert word_table.rows[2].cells[0].tables[0].rows[0].cells[1].text == "Header 2:"
    assert word_table.rows[2].cells[0].tables[0].rows[1].cells[0].text == "Data 1"
    assert word_table.rows[2].cells[0].tables[0].rows[1].cells[1].text == "Data 2"

if __name__ == "__main__":
    try:
        doc_obj: Document = Document(template_file)
    except:
        print(f"\rCould not open {template_file}. Please make sure it exists and is a valid Microsoft Word document. Exiting...\r")
        sys.exit(1)

    word_table = build_table(doc_obj, test_model)
    replace_placeholder_with_table(doc_obj, "{{py_placeholder}}", word_table)

    try:
        doc_obj.save(output_file)
        print(f"\n\nAs-Built document saved to {output_file}")
    except:
        print(f"\n\nCould not save output to {output_file}. If it is open please close and try again.\n\n")